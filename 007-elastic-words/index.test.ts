import { elasticize } from "./index"

describe ('Elasticize', () => {
    it ('Elasticize "ANNA" should return "ANNNNA"', function () {
        expect(elasticize("ANNA")).toEqual("ANNNNA")
    })
    it ('Elasticize "KAYAK" should return "KAAYYYAAK"', function () {
        expect(elasticize("KAYAK")).toEqual("KAAYYYAAK")
    })
    it ('Elasticize "X" should return "X"', function () {
        expect(elasticize("X")).toEqual("X")
    })
})
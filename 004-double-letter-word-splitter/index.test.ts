import {splitDoubleLetter} from "./index";

//Creating test block for
describe('splitDoubleLetter', () => {

    it('should split letter', () => {
        expect(splitDoubleLetter('letter')).toEqual(['let', 'ter'])
    })

    it('should split really', () => {
        expect(splitDoubleLetter('really')).toEqual(['real', 'ly'])
    })

    it ('should split Happy', () => {
        expect(splitDoubleLetter('happy')).toEqual(['hap','py'])
    })

    it('should split Shall', () => {
        expect(splitDoubleLetter('shall')).toEqual(['shal','l'])
    })

    it('should split Tool', () => {
        expect(splitDoubleLetter('tool')).toEqual(['to', 'ol'])
    })

    it('should split Mississippi', () => {
        expect(splitDoubleLetter('mississippi')).toEqual(['mis','sis','sip','pi'])
    })

    it ('should split Easy', () => {
        expect(splitDoubleLetter('easy')).toEqual([])
    })
})

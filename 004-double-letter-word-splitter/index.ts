//First attempt failed, so this is the solution by Dewald Els
export function splitDoubleLetter (word) {

    //checking if there's a word as input
    if (!word || !word.trim()) {
        throw new Error('No word was given')
    }

    // lager ein variabel for første indexen(karakteren) i stringen word
    let result = word[0]

    //starter for løkka på 1 ellers blir det kaos
    for (let i = 1; i < word.length; i++) {
        //sjekker så om to påfølgende indexer er like 
        if (word[i-1] === word[i]) {
            //Setter inn eit komma i ordet for å splitte det til to array
            result += ','
        }
        result += word[i];
    }

    //splitter ordet til to array der kommaet er
    const splitWords = result.split(',')

    //the word is not splitted == no double letter
    if(splitWords.length === 1) { 
        return []
    } else { 
        return splitWords
    }
}
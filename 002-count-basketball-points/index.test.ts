import {countPoints} from "./index"

it("should return 5", function () {
    expect(countPoints(1,1)).toBe(5)
})

it("should return 29", function(){
    expect(countPoints(7,5)).toBe(29)
})

it("should return 100", function(){
    expect(countPoints(38,8)).toBe(100)
})

it("should return 3", function(){
    expect(countPoints(0,1)).toBe(3)
})

it("should return 0", function(){
    expect(countPoints(0,0)).toBe(0)
})
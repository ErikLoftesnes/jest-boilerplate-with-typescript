//Function for counting basketball points
//Exporting function for use in test file
export function countPoints (twos, threes) {
    let totalPoints = 0;
    let totalTwoPoints = twos*2;
    let totalThreePoints = threes*3;
    totalPoints = totalTwoPoints + totalThreePoints;
    return totalPoints;
}
export function pigLatinSentence(string) {

    let array = string.split(' ')

    const vowels = '^[aieouAIEOU].*'

    let pigLatin = ''

    for (let word of array) {
        //check if the word starts with a vowel
        if (vowels.includes(word[0])){
            word += 'way'
        } else {
            let i = 0
            while (!vowels.includes(word[i])) {
                i++
            }
            word = word.substr(i) + word.substr(0,i) + 'ay'
        }
        pigLatin += word + ' '
    }
    return pigLatin.trim()
}

//While loop search for the first vowel in the word. 
//When the first vowel is found, the new word starts from the vowel with substring
//using substring, we can add the start of the word until the point of the first vowel
//at the end of the loop every modified word is added to the pigLatin string
//trim() to get rid of the empty space at the end
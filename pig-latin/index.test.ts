import {pigLatinSentence} from "./index"

describe ('Pig Latin Sentence', () => {

    it('should return "isthay isway igpay atinlay', function(){
        let sentence = "this is pig latin"
        expect(pigLatinSentence(sentence)).toEqual("isthay isway igpay atinlay")
    })
    it('should return allway eetstray ournaljay', function(){
        let sentence = "wall street journal"
        expect(pigLatinSentence(sentence)).toEqual("allway eetstray ournaljay")
    })
    it('should return aiseray ethay idgebray', function(){
        let sentence = "raise the bridge"
        expect(pigLatinSentence(sentence)).toEqual("aiseray ethay idgebray")
    })
    it('should return "allway igspay oinkway', function(){
        let sentence = "all pigs oink"
        expect(pigLatinSentence(sentence)).toEqual("allway igspay oinkway")
    })
})
export function maxProfit(array) {
    //Defining variables for important values
    let buy = 0;
    let sell = 0;
    let bestProfit = 0;

    for (let i = 0; i < array.length; i++) {
        //Calculating profit by taking the array[i] minus the first index
        let profit = (array[i] - array[buy]);

        //if the profit is larger than the best profit then
        if (profit > bestProfit) {
            sell = i;
            bestProfit = profit;
        } else if (array[i] < array[buy]) {
            //if the array[i] is smaller than our min index, the min index is changed to i
            // because if it's smaller we must check if it's a better buying time
           buy = i;
        }
    }

    if(bestProfit <= 0){
        return 0
    } else {
        return bestProfit
    }
   
}
import {maxProfit} from "./index"

describe ("Maximum profit should return 14", () => {
    
    it (" maximumProfit([8, 5, 12, 9, 19, 1]) should return 14", function () {
        expect(maxProfit([8, 5, 12, 9, 19, 1])).toBe(14)
    })

    it ("maxprofit ([2, 4, 9, 3, 8]} should return 7", function () {
        expect(maxProfit([2, 4, 9, 3, 8])).toBe(7)
    })

    it ("maxprofit ([21, 12, 11, 9, 6, 3]) should return 0", function () {
        expect(maxProfit([21, 12, 11, 9, 6, 3])).toBe(0)
    })
})


const months = {
  1: "A",
  2: "B",
  3: "C",
  4: "D",
  5: "E",
  6: "H",
  7: "L",
  8: "M",
  9: "P",
  10: "R",
  11: "S",
  12: "T",
};

const person = {
  name: "Matt",
  surname: "Edabit",
  gender: "M",
  dob: "1/1/1900",
};

export function fiscalCode(person) {
  //Removing vowels
  const consonants = (inputString) =>
    inputString.toUpperCase().replace(/[AEIOU]/g, "");

  //Removing all consonants
  const vowels = (inputString) =>
    inputString.toUpperCase().replace(/[^AEIOU]/g, "");

  //Creating a string of three letters and adding x if the name is not long enough
  const codiceFiscal = (inputString) =>
    (consonants(inputString) + vowels(inputString) + "XX").slice(0, 3);
    
  //Variable for splitting date of birth by / into three strings, d m y
  const [d, m, y] = person.dob.split("/");

  let surname = codiceFiscal(person.surname);
  let name = codiceFiscal(person.name);

  let year = y.slice(2);
  let month = months[m];
  let birth = ''

  if (person.gender === 'M' && d < 10) {
      birth = '0'+ d
  } else if (person.gender === 'M') {
      birth = d
  } else if (person.gender === 'F') {
      birth = d + 40
  }


  let fiscalFinale = surname + name + year + month + birth;


  console.log(surname);
  console.log(name);
  console.log(year);
  console.log(month);
  console.log(birth);
  

  console.log(fiscalFinale);
}

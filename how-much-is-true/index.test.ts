import {countTrue} from "./index" //ES Module

//Test code 
//3 phases in test driven development (red, green, blue)
// test() // can replace with "it(*argument that describes the test)"

it('should find 2 trues', function(){
    const bools = [true, false, false, true, false]
    //Expectation - important (JEST Framework) //assertions -
    expect(countTrue(bools)).toBe(2)
})

it('should find 0 trues', function(){
    const bools = [false, false, false, false]
    //Expectation - important (JEST Framework) //assertions -
    expect(countTrue(bools)).toBe(0)
})

it('should find 1 trues', function(){
    const bools = [1, 0, 0, true, false]
    //Expectation - important (JEST Framework) //assertions -
    expect(countTrue(bools)).toBe(1)
})

it('should find 1 trues', function(){
    const bools = ["true", 1, true, 0, false]
    //Expectation - important (JEST Framework) //assertions -
    expect(countTrue(bools)).toBe(1)
})

it('should find 0 trues', function(){
    const bools = []
    //Expectation - important (JEST Framework) //assertions -
    expect(countTrue(bools)).toBe(0)
})
import {shuffleCount} from './index'

describe ('shuffle', () => {
it('Deck of 8 cards should return 3', function () {
    expect(shuffleCount(8)).toBe(3)
})

})
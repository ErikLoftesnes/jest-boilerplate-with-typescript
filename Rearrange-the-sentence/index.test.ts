import {rearrange} from "./index"

describe ('Rearrange', () => {
    it('Rearrange "is2 Thi1s T4est 3a" should return "This is a Test"', function () {
        expect(rearrange("is2 Thi1s T4est 3a")).toEqual("This is a Test")
    })

    it('Rearrange "4of Fo1r pe6ople g3ood th5e the2" should return "For the good of the people"', function () {
        expect(rearrange("4of Fo1r pe6ople g3ood th5e the2")).toEqual("For the good of the people")
    })

    it('Rearrange "5weird i2s JavaScri1pt dam4n so3" should return "Javascript is so damn weird"', function() {
        expect(rearrange("5weird i2s JavaScri1pt dam4n so3")).toEqual("JavaScript is so damn weird")
    })

    it('Rearrange " " to be ""', function () {
        expect(rearrange(" ")).toEqual("")
    })
})
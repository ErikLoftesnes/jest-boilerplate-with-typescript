export function rearrange(sentence) {
    //Define array variable and remove spaces(trim) and splits the sentence on " "
    let arr = sentence.trim().split(" ");
    //Sorting the elements in the array. When using a, b, the function is applied to every element
    arr.sort((a, b) => {
        //Using parseInt to return the first integer. 
        //.replace regEx(all caracters that are not 0-9)
        return (
            parseInt(a.replace(/[^0-9]/g, "")) - parseInt(b.replace(/[^0-9]/g, ""))
        )
    })
    //map creates new array with the result of calling a function for each array element.
    //replacing all integers characters.
    return arr.map((a) => a.replace(/[0-9]/g, "")).join(" ")

}
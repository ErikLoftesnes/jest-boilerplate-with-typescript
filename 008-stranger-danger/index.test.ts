import {noStrangers} from "./index"

describe ('No strangers', () => {

    it('"See Spot run. See Spot jump. Spot likes jumping. See Spot fly" should return "spot, see"', function (){
        expect(noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.")).toEqual(["spot", "see"], [""])
    })
} )
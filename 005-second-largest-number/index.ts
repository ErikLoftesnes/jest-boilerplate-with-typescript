//Function for exporting the second largest number of an array
export function secondLargest(array) {

//Checking if the array is empty
    if (array.length === 0){
        return 0;
    }

//checking if the array only has one value
    if (array.length === 1){
        return array[0]
    }

//checking if tha array consists of two equal numbers
    if (array.length === 2) {
        for (let i = 0; i < array.length; i++) {
            if (array[i] === array[i + 1]) {
                return 0;
            }
        }
    }
    //Can do the above without for-loop
    /*if (array.length === 2 && array[0] === array[1]){
        return 0;
    }*/

//Sorting the array and returning the second largest number
    if (array.length > 2){
        const arraySorted = array.sort((a, b) => a - b);
        return arraySorted[array.length - 2];
    }
    }

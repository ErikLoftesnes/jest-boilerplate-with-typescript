import {secondLargest} from "./index"

describe ('secondLargest', () => {

    it('secondLargest([10, 40, 30, 20, 50]) should return 40', function() {
        expect(secondLargest([10, 40, 30, 20, 50])).toBe(40)
    })

    it('secondLargest([25, 143, 89, 13, 105]) should return 105', function() {
        expect(secondLargest([25, 143, 89, 13, 105])).toBe(105)
    })

    it('secondLargest([54, 23, 11, 17, 10]) should return 23', function() {
        expect(secondLargest([54, 23, 11, 17, 10])).toBe(23)
    })

    it('secondLargest([1, 1]) should return 0', function(){
        expect(secondLargest([1, 1])).toBe(0)
    })

    it('secondLargest([1]) should return 1', function(){
        expect(secondLargest([1])).toBe(1)
    })

    it('secondLargest([]) should return 0', function(){
        expect(secondLargest([])).toBe(0)
    })

})



/*secondLargest([10, 40, 30, 20, 50]) ➞ 40
secondLargest([25, 143, 89, 13, 105]) ➞ 105
secondLargest([54, 23, 11, 17, 10]) ➞ 23
secondLargest([1, 1]) ➞ 0
secondLargest([1]) ➞ 1
secondLargest([]) ➞ 0*/